package de.egore911.slowjdbc;

import java.sql.Connection;
import java.sql.ConnectionBuilder;
import java.sql.SQLException;
import java.sql.ShardingKey;

public class SlowConnectionBuilder implements ConnectionBuilder {

    private final ConnectionBuilder delegate;

    public SlowConnectionBuilder(ConnectionBuilder delegate) {
        this.delegate = delegate;
    }

    @Override
    public ConnectionBuilder user(String user) {
        return delegate.user(user);
    }

    @Override
    public ConnectionBuilder password(String password) {
        return delegate.password(password);
    }

    @Override
    public ConnectionBuilder shardingKey(ShardingKey shardingKey) {
        return delegate.shardingKey(shardingKey);
    }

    @Override
    public ConnectionBuilder superShardingKey(ShardingKey shardingKey) {
        return delegate.superShardingKey(shardingKey);
    }

    @Override
    public Connection build() throws SQLException {
        return new SlowConnection(delegate.build());
    }
}
